# php-arrange-files

PHP library for arranging files into directory structure based on criteria (like date modified), for example: 2018/12/31/01/0001.jpg

My outdoor security camera has a functionality that when a movement is detected, it can take a photo every second
and upload it to a server via FTP.

However:
1. the movement detection is not that good and it takes so many photos often unnecessarily.
2. it dumps all the photos in one folder and after a while the folder is flooded with 10,000's photos which makes
it hard to go through them.

I run the included example `arrange_by_YmdH.php` in a cron to arrange those thousands of photos into 
a directory structure like `2018/12/31/15/abc.jpg` (year/month/date/hour).

Check `arrange_by_YmdH.php` for an example of how to use this library.

