<?php

namespace OnlineSid\FileUtil;

class ArrangeFiles
{
    /**
     * ArrangeFiles constructor.
     *
     * @param string $dirSource
     * @param string $dirDestination
     * @param callable $arrangeFunc function ($fileName, $pathSeparator)
     * @param string $pathSeparator
     * @param bool $echo print output to indicate progress
     */
    function __construct($dirSource, $dirDestination,
         callable $arrangeFunc, $pathSeparator='/', $echo=true
    ) {
        $files = scandir($dirSource);
        $count = count($files);
        foreach ($files as $i => $fileName)
        {
            // skip . and ..
            if ($fileName == '.' || $fileName == '..') continue;

            // current file path
            $old = $dirSource.(substr($dirSource, -1) !== $pathSeparator ? $pathSeparator : '').$fileName;

            // skip if it's not a file (e.g.: skip if this is a directory
            if (!is_file($old)) continue;

            // destination directory
            $dir = $dirDestination.(substr($dirDestination, -1) !== $pathSeparator ? $pathSeparator : '').$arrangeFunc($old, $pathSeparator);

            // if destination directory does not exist, create it recursively
            if (!is_dir($dir)) mkdir($dir, 0750, true);

            // destination file path
            $new = $dir.$pathSeparator.$fileName;

            // move the file
            rename($old, $new);

            if ($echo) {
                echo "Processing $i out of $count files                     \r";
            }
        }
    }
}