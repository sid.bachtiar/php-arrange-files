<?php

/**
 * Arrange files by date('Y/m/d/H', $file_modified_time)
 */

require_once __DIR__.'/vendor/autoload.php';

function echoHelp() {
    echo <<<EOS
        Usage: php arrange_by_YmdH.php /dir/source /dir/destination

EOS;
}

$dirSource = $argv[1];
if (!$dirSource || !is_dir($dirSource)) {
    echo "Error: source directory is not a valid directory path: $dirSource \n";
    echoHelp();
    exit(1);
}

$dirDestination = $argv[2];
if (!$dirDestination || !is_dir($dirDestination)) {
    echo "Error: destination directory is not a valid directory path: $dirDestination \n";
    echoHelp();
    exit(1);
}

echo "Arrange files in $dirSource to $dirDestination ...\n";

new \OnlineSid\FileUtil\ArrangeFiles(
    $dirSource,
    $dirDestination,
    function ($filePath, $pathSeparator) {
        $time = filemtime($filePath);
        return date("Y".$pathSeparator."m".$pathSeparator."d".$pathSeparator."H", $time);
    }
);

echo "\n\n";